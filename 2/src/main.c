/*
 * Location of files should not be defined as constant, but rather passed as
 * an argument.
 * Further, should the hosts file be not specified, a default one should be
 * used from $XDG_DATA_HOME, or $XDG_DATA_DIRS.
 * Still further, it should be possible to pass the log file via stdin instead
 * of a file.
 * Most implementations of string.h are far from efficient, all dependencies on
 * it should be replaced with new functions.
 * Finally, right now the program assumes the correct input. It might be
 * worthwile to either correct the input directly, or execute the sed commands
 * every time the program is run.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>

#define MAXLINE		100
#define MAXHOSTS	5000
#define LOGFILE		"../data.fresh/dns.log"
#define BADHOSTS	"../data.fresh/hosts"

int getihost(char buff[], char host[][MAXLINE], int hctr);
int ctbadh(char baden[], int hctr, char hosts[][MAXLINE], int nhosts[]);

int main()
{
	FILE *fp;
	char buff[MAXLINE];
	char hosts[MAXHOSTS][MAXLINE];
	int nhosts[MAXHOSTS];
	int hctr  = 0;
	int ectr  = 0;
	int bectr = 0;

	fp = fopen(LOGFILE, "r");
	if (fp == NULL) {
		perror ("Error opening log");
		goto NOFILE;
	}
	while(fgets(buff, MAXLINE, fp) != NULL && hctr < MAXHOSTS) {
		++ectr;
		int ihost = getihost(buff, hosts, hctr);
		if (ihost == -1) {
			strcpy(hosts[hctr], buff);
			nhosts[hctr] = 1;
			++hctr;
		}
		else {
			++nhosts[ihost];
		}
	}
	fclose(fp);

	fp = fopen(BADHOSTS, "r");
	if (fp == NULL) {
		perror ("Error opening hosts");
		goto NOFILE;
	}
	while(fgets(buff, MAXLINE, fp) != NULL)
		bectr += ctbadh(buff, hctr, hosts, nhosts);
	fclose(fp);

	float p = bectr * 100.0 / ectr;
	printf("%2.2f%%\n", p);

	return 0;
	NOFILE: return ENOENT;
}

int ctbadh(char badh[], int hctr, char hosts[][MAXLINE], int nhosts[])
{
	int out = 0;
	for(int i = 0; i < hctr; ++i)
		if (strcmp(hosts[i], badh) == 0)
			out += nhosts[i];

	return out;
}

/* -1 if unique or i from matching hosts[i] */
int getihost(char buff[], char hosts[][MAXLINE], int hctr)
{
	int i = 0;
	while (i < hctr) {
		if (strcmp(buff, hosts[i]) == 0)
			return i;
		++i;
	}

	return -1;
}
